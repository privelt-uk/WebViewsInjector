package net.neferett.webviewsinjector.services.travelling;

import android.app.Activity;
import android.content.Context;
import android.view.KeyEvent;

import net.neferett.webviewsinjector.R;
import net.neferett.webviewsinjector.javascript.ElementValue;
import net.neferett.webviewsinjector.login.StepEnum;
import net.neferett.webviewsinjector.login.TypesAuthElement;
import net.neferett.webviewsinjector.response.ResponseEnum;
import net.neferett.webviewsinjector.services.LoginService;
import net.neferett.webviewsinjector.services.custom.CustomSelfInput;

import java.util.HashMap;

import lombok.SneakyThrows;

public class EasyJetService extends LoginService {

    /**
     *
     * @param context Main Activity Context
     */
    public EasyJetService(final Context context) {
        super("EasyJet", StepEnum.ONE_STEP, context, "https://www.easyjet.com/managebookings/en-GB", 4,
                R.drawable.ic_easyjet_logo, false, "https://www.easyjet.com/en/holidays/info/privacy-policy");
    }

    /**
     *
     * @param element TypeAuth element
     * @param name Name of Element©©
     */
    private void defineButton(TypesAuthElement element, String name) {
        this.step.addElement(element, new ElementValue(
                "getElementById", name, false
        ));
    }

    @Override
    public void setupElements() {
        this.step.addElement(TypesAuthElement.USERNAME, new ElementValue(
                "getElementById", "emailaddress", false
        ));

        this.step.addElement(TypesAuthElement.PASSWORD, new ElementValue(
                "getElementsByName", "password", true
        ));

        this.defineButton(TypesAuthElement.BUTTON_LOGIN, "sign-in-button");
        this.defineButton(TypesAuthElement.BUTTON_PASSWORD, "sign-in-button");
    }

    /**
     *
     * @return ResponseEnum and JavaScript Linked Code
     */
    @Override
    protected HashMap<ResponseEnum, String> setupTests() {
        return new HashMap<ResponseEnum, String>(){{
            put(ResponseEnum.FINISH_URL, "https://www.easyjet.com/managebookings/en-GB/en-GB/Manage");
            //put(ResponseEnum.BAD, "document.getElementsByClassName('sign-in-error')");
            put(ResponseEnum.BAD_PASSWORD, "document.getElementsByClassName('sign-in-error')");
            put(ResponseEnum.SUCCESS, "document.getElementsByClassName('llBookingSectionHeaderFat')[0].innerHTML");
        }};
    }
}
