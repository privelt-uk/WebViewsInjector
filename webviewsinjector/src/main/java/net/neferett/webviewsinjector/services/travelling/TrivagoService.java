/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package net.neferett.webviewsinjector.services.travelling;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.neferett.webviewsinjector.R;
import net.neferett.webviewsinjector.javascript.ElementValue;
import net.neferett.webviewsinjector.login.StepEnum;
import net.neferett.webviewsinjector.login.TypesAuthElement;
import net.neferett.webviewsinjector.response.ResponseCallback;
import net.neferett.webviewsinjector.response.ResponseEnum;
import net.neferett.webviewsinjector.services.LoginService;

import java.util.HashMap;
import java.util.Objects;

import lombok.SneakyThrows;

public class TrivagoService extends LoginService {

    private HashMap<TypesAuthElement, Object> typesAuthElementObjectHashMap;

    private ResponseCallback responseCallback;

    public TrivagoService(final Context context) {
        super("Trivago", StepEnum.TWO_STEP, context,
                "https://access.trivago.com/oauth/en-US/login", 3, R.drawable.trivago, false, "https://www.trivago.com/privacy-policy");
    }

    @Override
    public void setupElements() {
        if (this.typesAuthElementObjectHashMap == null)
            this.typesAuthElementObjectHashMap = new HashMap<>();

        this.step.addElement(TypesAuthElement.USERNAME, new ElementValue(
                "getElementById", "check_email", false
        ));

        this.step.addElement(TypesAuthElement.PASSWORD, new ElementValue(
                "getElementById", "login_password", false
        ));

        this.typesAuthElementObjectHashMap.put(
                TypesAuthElement.BUTTON_LOGIN, new ElementValue(
                        "getElementById", "login_email_submit", false
                ));
        this.typesAuthElementObjectHashMap.put(
                TypesAuthElement.BUTTON_PASSWORD, new ElementValue(
                        "getElementById", "login_submit", false
                ));
    }

    private KeyEvent[] getMappedEvent(String data) {
        return KeyCharacterMap.load(KeyCharacterMap.VIRTUAL_KEYBOARD).getEvents(data.toCharArray());
    }

    private void setupKeyEvents(String email, String password) {
        this.typesAuthElementObjectHashMap.put(
                TypesAuthElement.USERNAME,
                this.getMappedEvent(email)
        );
        this.typesAuthElementObjectHashMap.put(
                TypesAuthElement.PASSWORD,
                this.getMappedEvent(password)
        );
    }

    private WebViewClient createBookingClient() {
        TrivagoService instance = TrivagoService.this;

        return new WebViewClient() {
            private <T> T getObjectFromHash(TypesAuthElement element) {
                return (T) instance.typesAuthElementObjectHashMap.get(element);
            }

            private void injectingDataFromAuth(TypesAuthElement element) {
                for (KeyEvent event : this.<KeyEvent []>getObjectFromHash(element))
                    instance.getWebViewCreator().getWebView().dispatchKeyEvent(event);
            }

            private String constructJavaScriptMethod(TypesAuthElement element) {
                return this.<ElementValue>getObjectFromHash(element).clickValue();
            }

            private String getStringFromInjector(ResponseEnum responseEnum) {
                return TrivagoService.this.responseInjector.getResponses().get(responseEnum);
            }

            @SneakyThrows
            private void launchSignIn() {
                new java.util.Timer().schedule(
                        new java.util.TimerTask() {
                            @Override
                            public void run() {
                                ((Activity)instance.getContext()).runOnUiThread(() ->
                                        instance.webInjector.injectJavascript(constructJavaScriptMethod(TypesAuthElement.BUTTON_PASSWORD), null, null));
                            }
                        },
                        1000
                );
                new java.util.Timer().schedule(
                        new java.util.TimerTask() {
                            @Override
                            public void run() {
                                ((Activity)instance.getContext()).runOnUiThread(() ->
                                        injectingDataFromAuth(TypesAuthElement.PASSWORD));
                            }
                        },
                        5000
                );
                new java.util.Timer().schedule(
                        new java.util.TimerTask() {
                            @Override
                            public void run() {
                                ((Activity)instance.getContext()).runOnUiThread(() ->
                                        instance.webInjector.injectJavascript(constructJavaScriptMethod(TypesAuthElement.BUTTON_PASSWORD), null, null));
                            }
                        },
                        10000
                );
            }

            @SneakyThrows
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (!url.equalsIgnoreCase(TrivagoService.this.url))
                    return;

                // Injecting username at load
                injectingDataFromAuth(TypesAuthElement.USERNAME);
                // Clicking on next
                Thread.sleep(5000);
                instance.webInjector.injectJavascript(constructJavaScriptMethod(TypesAuthElement.BUTTON_LOGIN), null, null);
            }

            @Override
            public void doUpdateVisitedHistory(WebView webView1, String url, boolean isReload) {
                // We want here to build our response
                if (url.equalsIgnoreCase(this.getStringFromInjector(ResponseEnum.PASSWORD_URL)))
                    this.launchSignIn();
                else if (url.contains(this.getStringFromInjector(ResponseEnum.FINISH_URL)))
                    instance.responseCallback.getResponse(ResponseEnum.SUCCESS, "Success");
                else if (instance.responseCallback != null)
                    ((Activity)instance.getContext()).runOnUiThread(responseCallback);
            }
        };
    }

    @Override
    public void autoLogin(String email, String password, final ResponseCallback responseCallback) {
        this.load();
        this.setupKeyEvents(email, password);

        if (responseCallback != null)
            responseCallback.setResponseInjector(this.responseInjector);

        this.responseCallback = responseCallback;

        this.webViewCreator.applyClient(this.createBookingClient());
        new Handler().postDelayed(Objects.requireNonNull(responseCallback),
                ((delay * stepEnum.getMultiplicand())) * 1000);

        this.getWebInjector().load();
    }

    @Override
    protected HashMap<ResponseEnum, String> setupTests() {
        return new HashMap<ResponseEnum, String>() {{
            put(ResponseEnum.PASSWORD_URL, "https://access.trivago.com/oauth/en-US/login?step=login");
            put(ResponseEnum.FINISH_URL, "https://www.trivago.com");
            put(ResponseEnum.BAD_EMAIL, "document.getElementById('alert-window')");
            put(ResponseEnum.BAD_PASSWORD, "document.getElementById('alert-window')");
            put(ResponseEnum.SUCCESS, "document.getElementById('querytext')");
        }};
    }
}
