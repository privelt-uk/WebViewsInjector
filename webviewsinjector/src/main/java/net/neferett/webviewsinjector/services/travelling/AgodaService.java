/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package net.neferett.webviewsinjector.services.travelling;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import net.neferett.webviewsinjector.R;
import net.neferett.webviewsinjector.javascript.ElementValue;
import net.neferett.webviewsinjector.login.StepEnum;
import net.neferett.webviewsinjector.login.TypesAuthElement;
import net.neferett.webviewsinjector.response.ResponseEnum;
import net.neferett.webviewsinjector.services.LoginService;
import net.neferett.webviewsinjector.services.custom.CustomSelfInput;

import java.util.HashMap;

public class AgodaService extends LoginService {

    /**
     *
     * @param context Main Activity Context
     */
    public AgodaService(Context context) {
        super("Agoda", StepEnum.ONE_STEP, context, "https://www.agoda.com/en-gb/signin/email", 3, R.drawable.agoda, true, "https://www.agoda.com/info/privacy.html?cid=1844104");
    }

    @Override
    public void setupElements() {
        this.step.addElement(TypesAuthElement.USERNAME, new ElementValue(
                "querySelectorAll", ".iksYDU", true
        ));

        this.step.addElement(TypesAuthElement.PASSWORD, new ElementValue(
                "querySelectorAll", ".flrnRv", true
        ));

        this.step.addElement(TypesAuthElement.BUTTON_PASSWORD, new ElementValue(
                "querySelectorAll", ".hsJTpM", true
        ));
    }

    /**
     *
     * @return ResponseEnum and Linked JavaScript code
     */
    @Override
    protected HashMap<ResponseEnum, String> setupTests() {
        return new HashMap<ResponseEnum, String>(){{
            put(ResponseEnum.FINAL_EQUALS_URL, "https://www.agoda.com/en-gb/");
            put(ResponseEnum.BAD_EMAIL, "document.querySelector('.SimpleMessage__show--J0VfQ')");
            put(ResponseEnum.CAPTCHA, "document.getElementById('FunCaptcha')");
        }};
    }
}