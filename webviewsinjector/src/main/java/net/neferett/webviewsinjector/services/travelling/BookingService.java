/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package net.neferett.webviewsinjector.services.travelling;

import android.content.Context;
import net.neferett.webviewsinjector.R;
import net.neferett.webviewsinjector.javascript.ElementValue;
import net.neferett.webviewsinjector.login.StepEnum;
import net.neferett.webviewsinjector.login.TypesAuthElement;
import net.neferett.webviewsinjector.response.ResponseEnum;
import net.neferett.webviewsinjector.services.LoginService;

import java.util.HashMap;

public class BookingService extends LoginService {

    public BookingService(final Context context) {
        super("Booking", StepEnum.ZERO_STEP, context,
                "https://account.booking.com/applink/web?data=UrAJ6EOBPKICaehQpXjxi4oWUd2BnwELnZF62o33O6E3a2EVO6sMPmunw_e-pwxg41w34qvp84gxFRPOizFPuCjYCqar01oEaOjBlV7FnCKaiKbm94D0QK8J853vH6THI6XcUHqb8zxq7M3bSubTR0YvVneGXhIYBuXuEhFVD5OGcXxLh0o1Bi56NBy4wbz6tKyK9jjyFyAAdxqVf-MYHO0-BGcDXqpTMyQ3PMMgntMn5s6dap1tlk9dnS0hKc1CPpXF5E1iioMXR4Q_cazN6nVl1RVwhOMrpQI_kukzGsqUhZsMTzQQqgBnaty9hRchdBTH8TmmFbmY8s9jUDlyNhD3CsW91IpTPUm3ps-SsTgZvTOHgWFPx1P8xoZ3J_7pfzkDnOaseLARjDbMOGbB0C82uo2-YQd8Xiz-ofH_MYTQQWokf7tPEpOLuGpVydKhmWwP53WPSLdZmsN8VIY_1J-v7SRg9Xu9iX61bD6xJ7C2irhnvKB_S-a7c_83PJOGW4tSncR9EAOwzhSGB30J0G8lwNQDCT-L5uPG3AZNv078KVs1z6dIpadUBXwqCfyInu8UaaIEuQTjpIzkjmny38k4YaI17dHtxrrpyFSel0GTvwIL5ZkTJjVifN3_5NOsRoDG66Jp9Q4awRObLCPwE6i5g2vbR3_PTz60rYgZPjPLzu5U3_a74OzjSAYFSiokbnKN98PRHBC14oyH7F_GQE-8ZHnHt4O8zx3KPif1NH0IIuch9jwh92wPM2rQNP9fRAecScsb1ycFLhJjDDSSNTSzjah3QbJE9auQ0hzfjrb1mSQZ0dCEaRw-IfwFjcGVf7vB67HSHGzEhJvRDdia0J2WNT5y-yVf0YMlkk1PowCfU3i81mZkAtb6J_0uCh6hYVXLPg0ukFglodNJ3QW9_-qO4PakBGfg9XFfFOAvvdD4aKIy_hEWWaSmzwlnSomeoXDF2Q2i4EHbcixnXWrtlsDHwtcLwbuP5IV-CHJ1lTySy1SZOVs1TZCPvi_xoy2m5uzDm-1A7a2F_gP0_JsAOpCCclltezHfFUuvDaCuUPi4q54z9A1pr2XypNFf49f_NCmNKC5oF2A8_mspKHU8V5Jxmn7eA1xGXwjw3VvQYKfI5IaG8ZJdlorY4RTiYhN5zBklxH3wYDL4Dc0REeBt1GF3zJDqDBOooxaN4zV2lrf9zwuy-ty_8ryMZkvQryepu9QECxH0d9YxCSwL7_lnVunFSX0cstoMUpADijBLce-IFTP35df7vQA9VOdFNcFsk6JckrTwG_pxLKDyv73RosLZVcKx8ZCe419dSN3Ql9_GgmdT-_JpI15ZxQUvXpsbk86QO0uxow64lefS5HxX79Y0Xt2td0SGpSHTwuyd8uJkn7SbcxYw5mldJN0X1n2nTK4DHVL8G-S6kdWc0fiEpPgX3v-9y2EabIoUNmMXq8YOpE5jZUqjdEO-FFKWlrqlZcicmqd3jKwUKbZY7HEqy0X6fAA4qhiFrHlb64lgpu-Q8O94M3a-Bp7ioRjn1NHX46D5LWUliccQpeHGJAsRFcLVHoNuLns6hXVOE6D9tTJBtCsCNbZGUzIurcSEMhAyzGAC", 8, R.drawable.booking, false, "https://www.booking.com/content/privacy");
    }

    @Override
    public void setupElements() {
        this.step.addElement(TypesAuthElement.BUTTON_PASSWORD, new ElementValue(
                "querySelectorAll", ".nw-magic-link-sign-in-cta", true
        ));
    }

    @Override
    protected HashMap<ResponseEnum, String> setupTests() {
        return new HashMap<ResponseEnum, String>() {{
            put(ResponseEnum.FINISH_URL, "www.booking.com/index.fr.html");
            put(ResponseEnum.BAD_EMAIL, "document.getElementById('username-error')");
            put(ResponseEnum.BAD_PASSWORD, "document.getElementById('password-error')");
            put(ResponseEnum.SUCCESS, "document.getElementById('profile-menu-trigger--content')");
        }};
    }
}
